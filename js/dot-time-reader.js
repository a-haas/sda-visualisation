function render(plotData) {
    var plotDataKeys = Object.keys(plotData);
    var plotDataValues = Object.values(plotData);

    var thead = document.getElementById('measure-table').getElementsByTagName('thead')[0];
    var tbody = document.getElementById('measure-table').getElementsByTagName('tbody')[0];

    htmlHeader = "<tr> <th></th>";
    for(var i = 0; i < plotDataKeys.length; i++) {
        htmlHeader += "<th>" + plotDataKeys[i] + "</th>";
    }
    htmlHeader += "</tr>";
    thead.innerHTML = htmlHeader;

    htmlBody = "";
    var keys = Object.keys(plotDataValues[0]);
    for(var i = 0; i < keys.length; i++) {
        htmlBody += "<tr><td>" + keys[i] + "</td>";
        
        for(var j = 0; j < plotDataValues.length; j++) {
            htmlBody += "<td>" + plotDataValues[j][keys[i]] + "</td>";
        }
        
        htmlBody += "</tr>";
    }
    tbody.innerHTML = htmlBody;

    return plot('plot', plotDataKeys, plotDataValues);
}

function plot(id, plotDataKeys, plotDataValues) {
    var ctx = document.getElementById(id).getContext('2d');
    var palette = colorPalette(plotDataKeys.length);

    return new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            // we fetch the keys of one, ie. the first one, of the plotData
            labels: Object.keys(plotDataValues[0]),
            datasets: plotDataValues.map(function(it, i) {
                return {
                    label: plotDataKeys[i],
                    data: Object.values(it),
                    fill: false,
                    borderColor: palette[i],
                    backgroundColor: palette[i],
                }
            }),
        },
        options: {
            scales: {
                yAxes: [{
                    type: 'logarithmic',
                    ticks: {
                        min: 0,
                        max: 1000000,
                        callback: function (value, index, values) {
                            if (value === 1000000) return "1000s";
                            if (value === 100000) return "100s";
                            if (value === 10000) return "10s";
                            if (value === 1000) return "1s";
                            if (value === 100) return "100ms";
                            if (value === 10) return "10ms";
                            if (value === 0) return "0";
                            return null;
                        }
                   }
                },],
            },
        },
    });
}

var data = [];

// by default render data from C program
var renderPlot;

function changeHandler(evt) {
    evt.stopPropagation();
    evt.preventDefault();
 
    // FileList object.
    var files = evt.target.files;
 
    var file = files[0];
 
    var fileReader = new FileReader();
 
    fileReader.onloadstart = function(progressEvent) {
        newData = {};
        renderPlot && renderPlot.destroy();
    }
 
    fileReader.onload = function(progressEvent) {
        var content = fileReader.result;
        content.split('\n').forEach(function(line) {
            if(line) {
                newData[line.split(" ")[0]] = ((line.split(" ")[1] * 1000).toFixed(3));
            }
        });
        data[file.name] = newData;
    }
 
    fileReader.onloadend = function(progressEvent) {
        renderPlot = render(data);
    }
 
    fileReader.onerror = function(progressEvent) {}

    // Read file asynchronously.
    fileReader.readAsText(file, "UTF-8"); // fileReader.result -> String.
}

function colorPalette(i) {
    return [
        ["#003f5c",],
        ["#003f5c", "#ffa600",],
        ["#003f5c", "#bc5090", "#ffa600",],
        ["#003f5c", "#7a5195", "#ef5675", "#ffa600",],
        ["#003f5c", "#58508d", "#bc5090", "#ff6361", "#ffa600",],
        ["#003f5c", "#444e86", "#955196", "#dd5182", "#ff6e54", "#ffa600",],
        ["#003f5c", "#374c80", "#7a5195", "#bc5090", "#ef5675", "#ff764a", "#ffa600",],
        ["#003f5c", "#2f4b7c", "#665191", "#a05195", "#d45087", "#f95d6a", "#ff7c43", "#ffa600",],
    ][i - 1];
}