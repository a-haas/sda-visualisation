function render(abrInsertData, avlInsertData, abrWorstInsertData, avlWorstInsertData) {
    var rows = document.getElementById('measure-table').getElementsByTagName('tr');
    for(var i = 1; i < rows.length; i++) {
        rows[i].cells[1].innerText = abrInsertData[i-1] + " ms";
        rows[i].cells[2].innerText = avlInsertData[i-1] + " ms";
        rows[i].cells[3].innerText = abrWorstInsertData[i-1] + " ms";
        rows[i].cells[4].innerText = avlWorstInsertData[i-1] + " ms";
    }

    return plot('plot', [abrInsertData, avlInsertData, abrWorstInsertData, avlWorstInsertData]);
}

function plot(id, data) {
    var ctx = document.getElementById(id).getContext('2d');
    return new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: [10, 20, 40, 80, 160, 320, 640, 1280, 2560, 5120, 10240, 20480, 40960, 81920],
            datasets: [{
                label: 'BST average insertion execution time',
                data: data[0],
                fill: false,
                borderColor: "#003f5c",
                backgroundColor: "#003f5c",
            }, {
                label: 'AVL average insertion execution time',
                data: data[1],
                fill: false,
                borderColor: "#7a5195",
                backgroundColor: "#7a5195",
            }, {
                label: 'BST worst insertion execution time',
                data: data[2],
                fill: false,
                borderColor: "#ef5675",
                backgroundColor: "#ef5675",
            }, {
                label: 'AVL worst insertion execution time',
                data: data[3],
                fill: false,
                borderColor: "#ffa600",
                backgroundColor: "#ffa600",
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    type: 'logarithmic',
                    ticks: {
                        min: 0,
                        max: 1000000,
                        callback: function (value, index, values) {
                            if (value === 1000000) return "1000s";
                            if (value === 100000) return "100s";
                            if (value === 10000) return "10s";
                            if (value === 1000) return "1s";
                            if (value === 100) return "100ms";
                            if (value === 10) return "10ms";
                            if (value === 0) return "0";
                            return null;
                        }
                   }
                },],
            },
        },
    });
}

render(
    [0.004, 0.005, 0.006, 0.012, 0.031, 0.059, 0.093, 0.215, 0.444, 1.126, 2.841, 7.135, 22.942, 61.641],
    [0.004, 0.005, 0.006, 0.010, 0.017, 0.030, 0.039, 0.082, 0.158, 0.278, 0.497, 0.993, 1.935, 4.248],
    [0.003, 0.007, 0.016, 0.057, 0.215, 0.611, 1.663, 7.805, 33.469, 170.593, 927.353, 4570.302, 26763.029, 182551.382],
    [0.004, 0.006, 0.005, 0.011, 0.018, 0.028, 0.033, 0.072, 0.153, 0.263, 0.480, 1.006, 2.050, 4.296],
);