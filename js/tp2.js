function render(bubbleSortData, selectionSortData, insertionSortData, heapSortData) {
    var rows = document.getElementById('measure-table').getElementsByTagName('tr');
    
    for(var i = 1; i < rows.length; i++) {
        rows[i].cells[1].innerText = bubbleSortData[i-1] + " ms";
        rows[i].cells[2].innerText = selectionSortData[i-1] + " ms";
        rows[i].cells[3].innerText = insertionSortData[i-1] + " ms";
        rows[i].cells[4].innerText = heapSortData[i-1] + " ms";
    }

    return plot('plot', [bubbleSortData, selectionSortData, insertionSortData, heapSortData]);
}

function plot(id, data) {
    var ctx = document.getElementById(id).getContext('2d');
    return new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: [10, 20, 40, 80, 160, 320, 640, 1280, 2560, 5120, 10240, 20480, 40960, 81920],
            datasets: [{
                label: 'Bubble sort execution time',
                data: data[0],
                fill: false,
                borderColor: "#003f5c",
                backgroundColor: "#003f5c",
            }, {
                label: 'Selection sort execution time',
                data: data[1],
                fill: false,
                borderColor: "#7a5195",
                backgroundColor: "#7a5195",
            }, {
                label: 'Insertion sort execution time',
                data: data[2],
                fill: false,
                borderColor: "#ef5675",
                backgroundColor: "#ef5675",
            }
            , {
                label: 'Heap sort execution time',
                data: data[3],
                fill: false,
                borderColor: "#ffa600",
                backgroundColor: "#ffa600",
            },]
        },
        options: {
            scales: {
                yAxes: [{
                    type: 'logarithmic',
                    ticks: {
                        min: 0,
                        max: 1000000,
                        callback: function (value, index, values) {
                            if (value === 1000000) return "1000s";
                            if (value === 100000) return "100s";
                            if (value === 10000) return "10s";
                            if (value === 1000) return "1s";
                            if (value === 100) return "100ms";
                            if (value === 10) return "10ms";
                            if (value === 0) return "0";
                            return null;
                        }
                   }
                },],
            },
        },
    });
}

// by default render data from C program
var renderPlot = render(
    [0.004, 0.008, 0.019, 0.051, 0.108, 0.366, 1.029, 4.00, 16.563, 68.474, 312.673, 1408.018, 6865.537, 25285.061],
    [0.004, 0.007, 0.016, 0.051, 0.090, 0.238, 0.660, 2.534, 9.577, 38.516, 153.581, 615.684, 2754.483, 10509.274],
    [0.003, 0.005, 0.010, 0.023, 0.039, 0.125, 0.347, 1.370, 5.640, 20.374, 83.891, 338.375, 1802.822, 6029.318],
    [0.006, 0.007, 0.015, 0.033, 0.057, 0.109, 0.237, 0.512, 0.708, 1.287, 3.062, 6.342, 13.968, 29.960]
);