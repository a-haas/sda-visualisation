function render(id, matrixRandomData, adjRandomData, matrixFullData, adjFullData) {
    var rows = document.getElementById('measure-table-' + id).getElementsByTagName('tr');
    for(var i = 1; i < rows.length; i++) {
        rows[i].cells[1].innerText = matrixRandomData[i-1] + " ms";
        rows[i].cells[2].innerText = adjRandomData[i-1] + " ms";
        rows[i].cells[3].innerText = matrixFullData[i-1] + " ms";
        rows[i].cells[4].innerText = adjFullData[i-1] + " ms";
    }

    return plot('plot-' + id, [matrixRandomData, adjRandomData, matrixFullData, adjFullData]);
}

function plot(id, data) {
    var ctx = document.getElementById(id).getContext('2d');
    return new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: [10, 20, 40, 80, 160, 320, 640, 1280, 2560, 5120, 10240],
            datasets: [{
                label: 'Low density graph matrix execution time',
                data: data[0],
                fill: false,
                borderColor: "#003f5c",
                backgroundColor: "#003f5c",
            }, {
                label: 'Low density graph adjacency list execution time',
                data: data[1],
                fill: false,
                borderColor: "#7a5195",
                backgroundColor: "#7a5195",
            }, {
                label: 'High density graph matrix execution time',
                data: data[2],
                fill: false,
                borderColor: "#ef5675",
                backgroundColor: "#ef5675",
            }, {
                label: 'High density graph adjacency list execution time',
                data: data[3],
                fill: false,
                borderColor: "#ffa600",
                backgroundColor: "#ffa600",
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    type: 'logarithmic',
                    ticks: {
                        min: 0,
                        max: 1000000,
                        callback: function (value, index, values) {
                            if (value === 1000000) return "1000s";
                            if (value === 100000) return "100s";
                            if (value === 10000) return "10s";
                            if (value === 1000) return "1s";
                            if (value === 100) return "100ms";
                            if (value === 10) return "10ms";
                            if (value === 0) return "0";
                            return null;
                        }
                   }
                },],
            },
        },
    });
}

render(
    1,
    [0.078, 0.077, 0.088, 0.080, 0.077, 0.086, 0.077, 0.090, 0.088, 0.104, 0.113],
    [0.081, 0.084, 0.099, 0.093, 0.091, 0.087, 0.081, 0.109, 0.097, 0.143, 0.125],
    [0.085, 0.088, 0.081, 0.081, 0.088, 0.089, 0.093, 0.098, 0.112, 0.110, 0.111],
    [0.090, 0.091, 0.097, 0.132, 0.135, 0.320, 0.317, 0.516, 0.795, 1.636, 3.476],
);

render(
    2,
    [0.084, 0.089, 0.096, 0.096, 0.112, 0.218, 0.290, 0.562, 0.882, 1.670, 3.329],
    [0.087, 0.083, 0.086, 0.077, 0.070, 0.092, 0.082, 0.088, 0.082, 0.123, 0.094],
    [0.087, 0.095, 0.086, 0.090, 0.141, 0.213, 0.306, 0.561, 0.771, 1.670, 3.025],
    [0.089, 0.077, 0.078, 0.074, 0.075, 0.102, 0.095, 0.099, 0.106, 0.097, 0.088],
);

render(
    3,
    [0.002, 0.003, 0.007, 0.020, 0.097, 0.376, 1.277, 4.739, 18.672, 66.950, 264.315],
    [0.002, 0.003, 0.005, 0.007, 0.016, 0.041, 0.065, 0.252, 0.682, 1.002, 2.594],
    [0.001, 0.003, 0.008, 0.020, 0.096, 0.335, 1.340, 4.945, 18.793, 67.061, 259.772],
    [0.002, 0.004, 0.012, 0.036, 0.163, 0.742, 3.351, 11.460, 37.620, 131.224, 508.587],
);

render(
    4,
    [0.002, 0.003, 0.009, 0.026, 0.100, 0.328, 1.386, 4.761, 19.063, 66.659, 257.211],
    [0.002, 0.005, 0.006, 0.010, 0.019, 0.043, 0.117, 0.253, 0.398, 1.104, 2.701],
    [0.002, 0.003, 0.008, 0.027, 0.097, 0.343, 1.278, 4.879, 18.473, 66.262, 258.202],
    [0.002, 0.004, 0.011, 0.038, 0.138, 0.636, 2.909, 10.481, 35.946, 129.557, 546.168],
);