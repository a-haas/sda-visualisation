function render(listData, heapData) {
    var rows = document.getElementById('measure-table').getElementsByTagName('tr');
    for(var i = 1; i < rows.length; i++) {
        rows[i].cells[1].innerText = listData[i-1] + " ms";
        rows[i].cells[2].innerText = heapData[i-1] + " ms";
    }

    return plot('plot', [listData, heapData]);
}

function plot(id, data) {
    var ctx = document.getElementById(id).getContext('2d');
    return new Chart(ctx, {
        // The type of chart we want to create
        type: 'line',

        // The data for our dataset
        data: {
            labels: [10, 20, 40, 80, 160, 320, 640, 1280, 2560, 5120, 10240, 20480, 40960, 81920],
            datasets: [{
                label: 'Dijkstra linked list priority queue implementation',
                data: data[0],
                fill: false,
                borderColor: "#003f5c",
                backgroundColor: "#003f5c",
            }, {
                label: 'Dijkstra heap priority queue implementation',
                data: data[1],
                fill: false,
                borderColor: "#ffa600",
                backgroundColor: "#ffa600",
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    type: 'logarithmic',
                    ticks: {
                        min: 0,
                        max: 1000000,
                        callback: function (value, index, values) {
                            if (value === 1000000) return "1000s";
                            if (value === 100000) return "100s";
                            if (value === 10000) return "10s";
                            if (value === 1000) return "1s";
                            if (value === 100) return "100ms";
                            if (value === 10) return "10ms";
                            if (value === 0) return "0";
                            return null;
                        }
                   }
                },],
            },
        },
    });
}

render(
    [0.012, 0.029, 0.048, 0.073, 0.186, 0.474, 2.026, 10.060, 41.676, 170.727, 864.372, 5262.395, 24814.076, 120449.867],
    [0.020, 0.025, 0.053, 0.109, 0.089, 0.129, 0.278, 0.594, 1.344, 3.909, 10.035, 22.531, 48.163, 101.557],
);