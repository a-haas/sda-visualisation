Array.range = function(start, end) {
    return Array.apply(0, Array(end))
        .map((element, index) => index + start);
}

Array.random = function(length, max) {
    return Array.apply(null, Array(length)).map(function() {
        return Math.round(Math.random() * max);
    });
}

Array.prototype.swap = function (x,y) {
    var b = this[x];
    this[x] = this[y];
    this[y] = b;
    return this;
}

function minValueIndex(array, start) {
    var i;
    var minIndex = start;
    
    for(i = start; i < array.length; i++) {
        if(array[minIndex] > array[i]) {
            minIndex = i;
        }
    }

    return minIndex;
}